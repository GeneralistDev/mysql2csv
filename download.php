<?php
    include 'settings.php';

    $mysqli = new mysqli($MYSQL_HOST, $MYSQL_USER, $MYSQL_PASSWORD, $MYSQL_DB, $MYSQL_PORT);

    /* check connection */
    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    }

    date_default_timezone_set('America/Guayaquil');

    $date = date('m/d/Y-H:i:s', time());

    header('Content-Type: text/csv');
    header("Content-Disposition: attachment; filename=datos-$date.csv");
    header('Cache-Control: no-cache, no-store, must-revalidate');
    header('Pragma: no-cache');
    header('Expires: 0');

    $output = fopen('php://output', 'w');

    if ($result = $mysqli->query("SELECT * FROM mytable ORDER BY CreatedAt DESC")) {
        while ($row = $result->fetch_assoc()) {
            fputcsv($output, $row);
        }

        $result->close();
    }

    $mysqli->close();
?>
<?php
    if(count(get_included_files()) ==1) exit("Direct access not permitted.");

    $MYSQL_HOST = 'localhost';
    $MYSQL_PORT = 3306;
    $MYSQL_USER = 'exampleuser';
    $MYSQL_PASSWORD = 'password';
    $MYSQL_DB = 'example';
?>